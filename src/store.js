import { createStore } from 'redux';
import basicMathsReducer from './reducer';

const store = createStore(
    basicMathsReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;