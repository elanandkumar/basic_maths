import React from 'react';
import { Link } from "react-router-dom";

class NavigationBar extends React.PureComponent {
  render() {
    return (
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/addition">Addition</Link>
          </li>
          <li>
            <Link to="/subtraction">Subtraction</Link>
          </li>
          <li>
            <Link to="/multiplication">Multiplication</Link>
          </li>
          <li>
            <Link to="/division">Division</Link>
          </li>
        </ul>
      </nav>
    );
  }
}

export default NavigationBar;
