import React from "react";
import TextField from "@material-ui/core/TextField";

const TextInput = ({
  isReadOnly = false,
  value,
  onChange,
  onBlur,
  min,
  max,
  type,
  label,
  ...rest
}) => {
  if (isReadOnly) {
    return <div style={{ textAlign: "right", padding: '0 0 0 15px' }}>{value}</div>;
  }
  return (
    <TextField
      value={value}
      onChange={onChange}
      onBlur={onBlur}
      type={type || 'number'}
      label={label}
      inputProps={{
        min,
        max,
        style: {
            textAlign: type === 'text' ? 'right' : 'start'
        }
      }}
      {...rest}
    />
  );
};

export default TextInput;
