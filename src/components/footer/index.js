import React from 'react';

class Footer extends React.PureComponent {
    render(){
        return (
            <footer className="App-footer"><div>© 20020-2021 ElAnandKumar </div><div> All Rights Reserved</div></footer>
        )
    }
}

export default Footer;