import React from "react";
import {connect} from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';

const mapStateToProps = (state) => ({
  selectedOp: state.selectedOp && state.selectedOp.toUpperCase()
})

class Header extends React.PureComponent {
  render() {
    return (
      <AppBar position="static" style={{padding: "0 50px", marginBottom: '20px', display:'flex', flexDirection:'row', justifyContent: 'space-between'}}>
          <Typography variant="h6">
            Basic Maths: {this.props.selectedOp}
          </Typography>
          <a href="/">Back to homepage</a>
      </AppBar>
    );
  }
}

export default connect(mapStateToProps)(Header);
