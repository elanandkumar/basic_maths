import React from "react";
import { useParams } from "react-router-dom";
import TextInput from "../TextInput";
import { Paper } from "@material-ui/core";

const Question = ({ question, onAnswerSave }) => {
  const [answer, setAnswer] = React.useState(question && question.givenAnswer);
  const { questionId } = useParams();
  const inputRef = React.useRef();
  const handleAnswerChange = event => {
    setAnswer(event.target.value);
    if (event.target.setSelectionRange) {
      event.target.setSelectionRange(0, 0);
    }
  };

  const handleAnswerSave = () => {
    onAnswerSave(answer);
  };

  React.useEffect(() => {
      setAnswer("");
      inputRef.current && inputRef.current.focus();
  }, [questionId, inputRef]);

  if(!question){
      return <div>Something went wrong. <a href="/">Click here</a> to go to homepage</div>
  }
  return (
    <Paper style={{padding: "10px 20px"}}>
      <h3>{`Question ${questionId}`}</h3>
      {question.inputs.map((input, index) => (
        <TextInput key={index} value={input} isReadOnly />
      ))}
      <hr />
      <TextInput
        inputRef={inputRef}
        style={{width: '100%'}}
        type="text"
        onChange={handleAnswerChange}
        value={answer}
        variant="outlined"
        onBlur={handleAnswerSave}
        autoFocus
      />
    </Paper>
  );
};

export default Question;
