const basicMathsReducer = function(state={
    selectedOps: '',
    questions: [{
        id: '1',
        inputs: [123232, 34523232],
        answer: 123,
        givenAnswer: 123
    },{
        id: '1',
        inputs: [123, 345],
        answer: 123,
        givenAnswer: 345
    },{
        id: '1',
        inputs: [123, 345],
        answer: 123,
        givenAnswer: 345
    },{
        id: '1',
        inputs: [123, 345],
        answer: 123,
        givenAnswer: 345
    },{
        id: '1',
        inputs: [123, 345],
        answer: 123,
        givenAnswer: 345
    }]
}, action) {
    switch(action.type) {
        case 'CHANGE_SELECT_OP':
            return {...state, selectedOp: action.payload};
        case 'CREATE_QUESTIONS':
            return createQuestions(state, action.payload);
        case 'SET_GIVEN_ANSWER':
            const { index, givenAnswer } = action.payload;
            const questions = state.questions.map((question, ind) => {
                if(ind === index) {
            question.givenAnswer = givenAnswer;
                    return {...question, givenAnswer: +givenAnswer}
                }
                return question
            });
            return {...state, questions: [...questions]}
        default:
            return state;
    }
};

function createQuestions(state, payload) {
    if(state.selectedOp === 'addition') {
        return createAdditions(state, payload);
    }
}

function createAdditions(state, {digits, totalQuestions, rows}){
    const start = '1'.padEnd(digits, '0'); // str1.padEnd(25, '.')
    const end = '9'.padEnd(digits, '0');
    const questions = [];
    for(let j = 0; j < totalQuestions; j++) {
      const question = {
        id: `question_${j+1}`,
        inputs: [],
        givenAnswer: '',
        answer: 0
      };
      for(let i = 0; i < rows; i++) {
        const nextValue = Math.floor(Number(start) + Math.random() * Number(end));
        question.inputs.push(nextValue);
        question.answer += nextValue;
      }
      questions.push(question);
    }
    return {...state, questions}
}

export default basicMathsReducer;