import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/home";
import Header from "./components/header";
import Footer from "./components/footer";
import Addition from "./pages/addition";
import Subtraction from "./pages/subtraction";
import Multiplication from "./pages/multiplication";
import Division from "./pages/division";
import Questions from "./pages/questions";
import Results from "./pages/results";
import Container from '@material-ui/core/Container';

function App() {
  return (
    <>
      <Header />
      <Container fixed>
          <Router>
            <Switch>
              <Route path="/addition">
                <Addition />
              </Route>
              <Route path="/subtraction">
                <Subtraction />
              </Route>
              <Route path="/multiplication">
                <Multiplication />
              </Route>
              <Route path="/division">
                <Division />
              </Route>
              <Route path="/questions">
                <Questions />
              </Route>
              <Route path="/results">
                <Results />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </Router>
        </Container>
      <Footer />
    </>
  );
}

export default App;
