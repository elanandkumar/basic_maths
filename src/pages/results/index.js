import React from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import TextInput from "../../components/TextInput";

const mapStateToProps = state => ({
  questions: state.questions,
  totalCorrect: state.questions.filter(question => question.answer === question.givenAnswer).length
});

class Results extends React.PureComponent {
    isCorrect = (question) => {
        return question.givenAnswer === question.answer
    }
  render() {
    const { questions, totalCorrect } = this.props;

    return (
      <Grid direction="column" container>
        <Grid item>
            <h3>{`Score: ${totalCorrect}/${questions.length}`}</h3>
        </Grid>
        <Grid container>
          {questions.map(question => {
            const isCorrect = this.isCorrect(question);
            return (
              <Grid item xs={5} sm={3} style={{margin: '0 10px', padding: '10px', border: `2px solid ${isCorrect ? 'green': 'red'}`}}>
                <div style={{ position: 'absolute', color: `${isCorrect ? 'green' : 'red' }` }}>
                  {isCorrect ? "✓" : "✘"}
                </div>
                {question.inputs.map(input => (
                  <TextInput value={input} isReadOnly />
                ))}
                <hr />
                <TextInput value={question.givenAnswer || 0} isReadOnly />
                <hr />
                <TextInput value={question.answer} isReadOnly />
              </Grid>
            );
          })}
        </Grid>
      </Grid>
    );
  }
}

export default connect(mapStateToProps)(Results);
