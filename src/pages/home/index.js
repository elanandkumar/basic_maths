import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Radio from "@material-ui/core/Radio";
import Grid from "@material-ui/core/Grid";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";

const mapStateToProps = state => ({
  selectedOp: state.selectedOp
});

const mapDispatchToProps = dispatch => {
  return {
    handleOpChange: selectedOp =>
      dispatch({
        type: "CHANGE_SELECT_OP",
        payload: selectedOp
      })
  };
};

const Home = function({ selectedOp, handleOpChange }) {
  const handleChange = event => {
    handleOpChange(event.target.value);
  };

  const handleLinkClick = event => {
    if (!selectedOp) {
      event.preventDefault();
    }
  };

  return (
    <Grid container direction="column" spacing={3}>
      <Grid item xs={3}>
        <FormControl component="fieldset">
          <RadioGroup
            aria-label="basic-maths-op"
            name="basic-maths-op"
            value={selectedOp}
            onChange={handleChange}
          >
            <FormControlLabel
              value="addition"
              control={<Radio />}
              label="Addition"
            />
            <FormControlLabel
              value="subtraction"
              control={<Radio />}
              label="Subtraction"
            />
            <FormControlLabel
              value="multiplication"
              control={<Radio />}
              label="Multiplication"
            />
            <FormControlLabel
              value="division"
              control={<Radio />}
              label="Division"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid item xs={3}>
        <Link to={`/${selectedOp}`} onClick={handleLinkClick}>
          <Button variant="contained" color="primary" disabled={!selectedOp}>
            Next
          </Button>
        </Link>
      </Grid>
    </Grid>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
