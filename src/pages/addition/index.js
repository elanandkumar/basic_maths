import React from "react";
import { connect } from "react-redux";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import TextInput from "../../components/TextInput";

const MAX_DIGITS = 9;
const MAX_ROWS = 5;

const mapDispatchToProps = dispatch => ({
  setQuestions: (totalQuestions, digits, rows) =>
    dispatch({
      type: "CREATE_QUESTIONS",
      payload: {
        totalQuestions,
        digits,
        rows
      }
    })
});

const Addition = function({ setQuestions }) {
  const addRef = React.createRef();
  const history = useHistory();
  const [totalQuestions, setTotalQuestions] = React.useState(1);
  const [digits, setDigits] = React.useState(1);
  const [rows, setRows] = React.useState(2);

  const handleTotalQuestionChange = event => {
    setTotalQuestions(event.target.value);
  };

  const handleDigitsValueChange = event => {
    const value = event.target.value;
    if (value && value <= MAX_DIGITS) {
      setDigits(event.target.value);
    }
  };
  const handleRowValueChange = event => {
    const value = event.target.value;
    if (value && value <= MAX_ROWS) {
      setRows(event.target.value);
    }
  };

  const handleOnClick = () => {
    if (addRef.current.checkValidity()) {
      setQuestions(totalQuestions, digits, rows);
      history.push("/questions");
    } else {
      addRef.current.reportValidity();
    }
  };
  return (
    <Grid container direction="column" spacing={3}>
      <Grid item>
        <form ref={addRef}>
          <FormControl component="fieldset" style={{ width: "100%" }}>
            <TextInput
              label="Number of questions"
              value={totalQuestions}
              onChange={handleTotalQuestionChange}
              min="1"
              max="30"
              autoFocus
              required
            />
            <TextInput
              label="Number of digits"
              value={digits}
              onChange={handleDigitsValueChange}
              min="1"
              max="15"
              required
            />
            <TextInput
              label="Number of rows"
              value={rows}
              onChange={handleRowValueChange}
              min="2"
              max="5"
              required
            />
          </FormControl>
        </form>
      </Grid>
      <Grid item xs={3}>
        <Button variant="contained" color="primary" onClick={handleOnClick}>
          Create
        </Button>
      </Grid>
    </Grid>
  );
};

export default connect(null, mapDispatchToProps)(Addition);
