import React from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { useRouteMatch, Switch, Route } from "react-router-dom";
// import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";
import Question from "../../components/question";

const mapStateToProps = state => ({
  totalQuestions: state.questions.length,
  questions: state.questions
});

const mapDispatchToProps = dispatch => ({
  setGivenAnswer: (givenAnswer, index) =>
    dispatch({
      type: "SET_GIVEN_ANSWER",
      payload: {
        givenAnswer,
        index
      }
    })
});

const Questions = function({ totalQuestions, questions, setGivenAnswer }) {
  const [count, setCount] = React.useState(1);
  const [label, setLabel] = React.useState("Start");
  const [isDisabled, setIsDisabled] = React.useState(true);
  // const history = useHistory();

  const handleNext = () => {
    if (count <= totalQuestions) {
      setCount(count + 1);
      setLabel("Next");
    }
  };

  const handleAnswerSave = answer => {
    setIsDisabled(false);
    setGivenAnswer(answer, count - 2);
  };

  React.useEffect(() => {
    setIsDisabled(true);
  }, [count]);

  const { path } = useRouteMatch();
  const nextLinkTo =
    count <= totalQuestions ? `/questions/${count}` : "/results";

  return (
    <Grid direction="column" container spacing={3}>
      <Grid item>
        <Switch>
          <Route exact path={path}>
            Are you ready?
          </Route>
          <Route path={`${path}/:questionId`}>
            <Question
              question={questions[count - 2]}
              onAnswerSave={handleAnswerSave}
            />
          </Route>
        </Switch>
      </Grid>
      <Grid item xs={3}>
        <Link to={nextLinkTo}>
          <Button
            variant="contained"
            color="primary"
            onClick={handleNext}
            disabled={label !== "Start" && isDisabled}
          >
            {label}
          </Button>
        </Link>
      </Grid>
    </Grid>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Questions);
